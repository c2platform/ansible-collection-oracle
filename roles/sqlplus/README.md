# Ansible Role c2platform.oracle.sqlplus

Install [SQL*Plus](http://www.oracle.com/technology/tech/sql_plus/) using [Instant Client RPMs](https://www.oracle.com/database/technologies/instant-client.html) from [public-yum.oracle.com](http://public-yum.oracle.com).

- [Requirements](#requirements)
- [Role Variables](#role-variables)
- [Dependencies](#dependencies)
- [Example](#example)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

* [Oracle public Yum repository](https://yum.oracle.com/) should be enabled. See example below.

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

The list `sqlplus_packages` is used to install SQL*Plus using RPMs. Default it is configured as follows:

```yaml
sqlplus_packages:
  - name: oracle-instantclient-release-el7
  - name: oracle-instantclient-sqlplus
```

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

The play below uses the `c2platform.core.common` role to enable the public Oracle Yum repository. This role allows Yum repositories to be added / removed using list `yum_repositories`.

```yaml
- name: oracle
  hosts: oracle
  become: yes

  roles:
    - { role: c2platform.core.common, tags: ["common"] }
    - { role: c2platform.oracle.sqlplus, tags: ["oracle", "sqlplus"] }

  vars:
    yum_repositories:
    - name: public_ol7_latest
        description: Oracle Linux $releasever Latest ($basearch)
        baseurl: http://public-yum.oracle.com/repo/OracleLinux/OL7/latest/$basearch/
        gpgkey: http://public-yum.oracle.com/RPM-GPG-KEY-oracle-ol7
        gpgcheck: yes
```

See also the reference project play [plays/mw/oracle.yml](https://gitlab.com/c2platform/ansible/-/tree/master/plays/mw/oracle.yml) and vars in [group_vars/oracle/main.yml](https://gitlab.com/c2platform/ansible/-/blob/master/group_vars/oracle/main.yml).