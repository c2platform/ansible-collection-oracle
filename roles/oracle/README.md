# Ansible Role c2platform.oracle.oracle

This role can be used to create Oracle users and grant privileges. This role used modules that are provided by the Ansible collection [ari_stark.ansible_oracle_modules](https://galaxy.ansible.com/ari_stark/ansible_oracle_modules). This collection uses [cx_Oracle](https://cx-oracle.readthedocs.io) as interface to the Oracle database.

- [Requirements](#requirements)
- [Role Variables](#role-variables)
- [Dependencies](#dependencies)
- [Example](#example)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

* Python library [cx_Oracle](https://cx-oracle.readthedocs.io)

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

This role uses list `oracle_users` and `oracle_grants` to create users and assign privileges.

```yaml
oracle_users:
  - schema_name: c1
    schema_password: secret
    username: sys
    password: secret
    mode: sysdba
    hostname: c2d-oracle1
    port: 1521
    service_name: c2d
    # state: absent

oracle_grants:
  - grantee: c1
    privileges:
      - create session
      - create table
    username: sys
    password: secret
    mode: sysdba
    hostname: c2d-oracle1
    port: 1521
    service_name: c2d
```

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

* [ari_stark.ansible_oracle_modules](https://galaxy.ansible.com/ari_stark/ansible_oracle_modules) should be enabled. See example below.

## Example

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

The play below uses the `c2platform.core.common` role to enable the public Oracle Yum repository. This role allows Yum repositories to be added / removed using list `yum_repositories`.

```yaml
- name: oracle
  hosts: oracle
  become: yes

  roles:
    - { role: c2platform.core.common, tags: ["common"] }
    - { role: c2platform.oracle.oracle, tags: ["oracle"] }
```

See also the reference project play [plays/mw/oracle.yml](https://gitlab.com/c2platform/ansible/-/tree/master/plays/mw/oracle.yml) and vars in [group_vars/oracle/main.yml](https://gitlab.com/c2platform/ansible/-/blob/master/group_vars/oracle/main.yml).