# CHANGELOG

## 1.0.0 ( 2023-06-16 )

* New pipeline script.
* [oracle](./roles/oracle) `oracle_roles`, `oracle_dirs` and `oracle_tablespaces`.

## 0.1.0 ( 2022-11-09 )

* [sqlplus](./roles/sqplus) new role.
* [oracle](./roles/oracle) new role.
