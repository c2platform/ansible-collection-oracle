# Ansible Collection - c2platform.oracle

[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-oracle/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-oracle/-/pipelines) [![Latest Release](https://gitlab.com/c2platform/ansible-collection-oracle/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-oracle/-/pipelines)

See full [README](https://gitlab.com/c2platform/ansible-collection-oracle/-/blob/master/README.md).
